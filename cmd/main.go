package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/pdamon/fargoizer/cmd/htmx"
)

func main() {
	// setup logging
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	log.Info().Msg("Logging Initiated")

	// setup handling of SIGTERM/SIGINT
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-c
		log.Info().Msg("Received Termination Signal. Exiting...")
		os.Exit(1)
	}()

	// set server default values
	const defaultTimeoutSeconds = 10
	const defaultPort = 8080

	// create a file server
	fs := http.FileServer(http.Dir("./html"))

	// set config flags
	portPtr := flag.Int("PortNumb", defaultPort, "The port number to run the server on")

	// setup http router
	router := mux.NewRouter()

	router.Handle("/", fs)
	// router.HandleFunc("/bracket", GetBracket)
	router.HandleFunc("/generate-bracket", htmx.GetBracketStructure)
	router.HandleFunc("/solve-bracket", htmx.SolveBracket)

	srv := &http.Server{
		ReadHeaderTimeout: time.Duration(defaultTimeoutSeconds * float64(time.Second)),
		Addr:              fmt.Sprintf("0.0.0.0:%d", *portPtr),
		Handler:           router,
	}

	log.Info().Msg(fmt.Sprintf("Now listening at Address: %s", srv.Addr))

	err := srv.ListenAndServe()

	log.Error().Msg(fmt.Sprintf("Error Serving Content: %s", err))
}
