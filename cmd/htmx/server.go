package htmx

import (
	"fmt"
	"math"
	"net/http"
	"sort"
	"strconv"

	"github.com/a-h/templ"
	"github.com/rs/zerolog/log"
	"gitlab.com/pdamon/fargoizer/cmd/tournament"
)

const NUM_SIMS = 100_000

type parsedFormData struct {
	numPlayers  uint
	numPlaces   uint
	losersOrder [][]uint32
	places      []string
	payouts     []float64
	bracket     tournament.Bracket
}

func GetBracketStructure(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		if err := r.ParseForm(); err != nil {
			http.Error(w, "Failed to parse input", http.StatusBadRequest)
			return
		}

		dropDownValue, err := strconv.Atoi(r.FormValue("numPlayers"))

		if err != nil {
			http.Error(w, "Failed to parse input", http.StatusBadRequest)
			return
		}

		var component templ.Component

		switch dropDownValue {
		case 4:
			component = fourPlayers()
		case 8:
			component = eightPlayers()
		case 16:
			component = sixteenPlayers()
		case 32:
			component = thirtyTwoPlayers()
		case 64:
			component = sixtyFourPlayers()
		// case 128:
		// 	component = oneHundredTwentyEightPlayers()
		default:
			log.Error().Msg("Invalid dropdown menu value")
		}

		component.Render(r.Context(), w)
	} else {
		log.Warn().Msg("Received request with invalid method. Only 'Post' is accepted.")
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func returnErrorStatus(msg string, r *http.Request, w *http.ResponseWriter) {
	component := errorDiv(msg)
	component.Render(r.Context(), *w)
}

func parseRequest(r *http.Request) (parsedFormData, error) {
	err := r.ParseForm()
	if err != nil {
		return parsedFormData{}, fmt.Errorf("error parsing form")
	}

	numPlayers, err := strconv.Atoi(r.FormValue("field-size"))
	if err != nil {
		return parsedFormData{}, fmt.Errorf("error getting field size from input data")
	}

	var numPlaces uint
	switch numPlayers {
	case 4:
		numPlaces = 6
	case 8:
		numPlaces = 8
	case 16:
		numPlaces = 10
	case 32:
		numPlaces = 12
	case 64:
		numPlaces = 14
	default:
		log.Error().Msg(fmt.Sprintf("Invalid number of players was found. Length of numPlayers was %d", numPlayers))
		return parsedFormData{}, fmt.Errorf("error matching number of players")
	}

	log.Info().Msg(fmt.Sprintf("Got request to solve bracket with %d players, resulting in %d places.", numPlayers, numPlaces))

	winnerRace, err := strconv.Atoi(r.FormValue("winners-race"))
	if err != nil {
		return parsedFormData{}, fmt.Errorf("error converting 'winners-race' value of '%s' to an int", r.FormValue("winners-race"))
	}

	losersRace, err := strconv.Atoi(r.FormValue("losers-race"))
	if err != nil {
		return parsedFormData{}, fmt.Errorf("error converting 'losers-race' value of '%s' to an int", r.FormValue("losers-race"))
	}

	spotDiff, err := strconv.Atoi(r.FormValue("spot-diff"))
	if err != nil {
		return parsedFormData{}, fmt.Errorf("error converting 'spot-diff' value of '%s' to an int", r.FormValue("spot-diff"))
	}

	maxSpot, err := strconv.Atoi(r.FormValue("max-spot"))
	if err != nil {
		return parsedFormData{}, fmt.Errorf("error converting 'max-spot' value of '%s' to an int", r.FormValue("max-spot"))
	}

	addRace := false
	if r.FormValue("add-race") == "Add" {
		addRace = true
	}

	if !addRace && (maxSpot >= losersRace) {
		return parsedFormData{}, fmt.Errorf("max spot was %d, which must be less than %d losersRace when addRace is false", maxSpot, losersRace)
	}

	places, payouts, err := getPayouts(r)
	if err != nil {
		return parsedFormData{}, fmt.Errorf("error getting tournament payouts")
	}

	raceCalc := tournament.CreateRaceCalc(uint(winnerRace), uint(losersRace), uint(spotDiff), uint(maxSpot), addRace)
	losersOrder, err := makeLosersOrders(r, uint(numPlayers))

	ws := tournament.NewRandWinSelector(0)

	if err != nil {
		log.Error().Msg(err.Error())
		return parsedFormData{}, fmt.Errorf("error making the losers orders")
	}

	return parsedFormData{
		numPlayers:  uint(numPlayers),
		numPlaces:   numPlaces,
		losersOrder: losersOrder,
		places:      places,
		payouts:     payouts,
		bracket:     tournament.NewBracket(ws, raceCalc),
	}, nil

}

const DEFAULT_NUM_ROUNDS = 100_000

func SolveBracket(w http.ResponseWriter, r *http.Request) {
	parsedData, err := parseRequest(r)
	if err != nil {
		log.Error().Msg(err.Error())
		returnErrorStatus("Error parsing request.", r, &w)
		return
	}

	players, _ := createPlayers(r, parsedData.numPlayers)
	losersSide := []tournament.Player{}

	for i := 0; i < len(players)/2; i++ {
		losersSide = append(losersSide, tournament.ByePlayer())
	}

	log.Trace().Msg("Created following losers order")
	for i := 0; i < len(parsedData.losersOrder); i++ {
		log.Trace().Msg(fmt.Sprintf("Losers order round %d", i+1))
		log.Trace().Msg(fmt.Sprintf("%v", parsedData.losersOrder[i]))
	}

	round := tournament.NewRound(players, losersSide)

	percentages, err := parsedData.bracket.Sim(round, parsedData.losersOrder, false, DEFAULT_NUM_ROUNDS)
	if err != nil {
		component := errorDiv("Error running simulation")
		component.Render(r.Context(), w)
		return
	}

	field_percentages, err := parsedData.bracket.Sim(round, parsedData.losersOrder, true, DEFAULT_NUM_ROUNDS)
	if err != nil {
		component := errorDiv("Error running simulation")
		component.Render(r.Context(), w)
		return
	}

	for i := 0; i < len(percentages[0])-len(parsedData.places); i++ {
		parsedData.payouts = append(parsedData.payouts, 0)
	}

	evs, err := CalcEVs(percentages, parsedData.payouts)
	if err != nil {
		component := errorDiv("Error running simulation")
		component.Render(r.Context(), w)
		return
	}
	field_evs, err := CalcEVs(field_percentages, parsedData.payouts)
	if err != nil {
		component := errorDiv("Error running simulation")
		component.Render(r.Context(), w)
		return
	}

	sortedPlayers := SortPlayersByEv(players, field_evs)

	component := resultsGrid(sortedPlayers, parsedData.places, percentages, field_percentages, evs, field_evs)
	component.Render(r.Context(), w)

	log.Trace().Msg("Finished solving bracket")
}

func CalcEVs(percentages [][]float64, payouts []float64) ([]float64, error) {
	evs := make([]float64, len(percentages))

	for p := 0; p < len(percentages); p++ {
		if len(payouts) != len(percentages[p]) {
			err := fmt.Errorf("error calculating evs. len of evs %d did not match len percentages %d", len(payouts), len(percentages[p]))
			log.Err(err).Msg("error calculating evs")
			return nil, err
		}

		sum := 0.0
		for i := 0; i < len(payouts); i++ {
			sum += percentages[p][i] * payouts[i]
		}

		evs[p] = sum
	}

	return evs, nil
}

func createPlayers(r *http.Request, numPlayers uint) ([]tournament.Player, uint) {
	players := make([]tournament.Player, numPlayers)
	numRealPlayers := uint(0)

	for i := 0; i < int(numPlayers); i++ {
		name := r.Form[fmt.Sprintf("PlayerName%d", i)][0]
		fargo_str := r.Form[fmt.Sprintf("PlayerRating%d", i)][0]

		fargo, err := strconv.Atoi(fargo_str)

		if err != nil {
			log.Error().Msg(fmt.Sprintf("There was an error converting the fargo %s of player %s to an int.", name, fargo_str))
			players[i] = tournament.ByePlayer()
		} else {
			numRealPlayers++
			players[i] = tournament.Player{
				Name:  name,
				Fargo: fargo,
				Id:    numRealPlayers,
			}

			log.Trace().Msg(fmt.Sprintf("Created player %s, fargo %d, id %d in location %d", name, fargo, numRealPlayers, i))
		}
	}

	return players, numRealPlayers
}

type Pair struct {
	ev  float64
	idx int
}

func SortPlayersByEv(players []tournament.Player, evs []float64) []tournament.Player {
	output := make([]tournament.Player, len(players))
	pairs := make([]Pair, len(players))

	for i := 0; i < len(players); i++ {
		pairs[i] = Pair{ev: evs[players[i].Id], idx: i}
	}

	sort.Slice(pairs, func(i, j int) bool {
		if math.Abs(float64(pairs[j].ev-pairs[i].ev)) < 0.001 {
			return pairs[i].idx < pairs[j].idx
		}
		return pairs[j].ev < pairs[i].ev
	})

	for i := 0; i < len(players); i++ {
		output[i] = players[pairs[i].idx]
	}

	return output
}

func makeLosersOrders(r *http.Request, numPlayers uint) ([][]uint32, error) {
	numRounds := int(math.Round(math.Log2(float64(numPlayers)))) - 1
	output := make([][]uint32, numRounds)

	for round := 1; round <= numRounds; round++ {
		numFields := int(math.Pow(2.0, float64(numRounds-round+1)))
		localOrder := make([]uint32, numFields)
		min := uint32(math.MaxUint32)

		for field := 0; field < numFields; field++ {
			val, err := strconv.ParseUint(r.FormValue(fmt.Sprintf("LoserMatch-%d-%d", round, field)), 10, 32)

			if err != nil {
				return [][]uint32{}, fmt.Errorf("error parsing field 'LoserMatch-%d-%d'", round, field)
			}

			val32 := uint32(val)
			localOrder[field] = val32
			if val32 < min {
				min = val32
			}
		}

		for i := range localOrder {
			localOrder[i] -= min
		}
		output[round-1] = localOrder
	}

	return output, nil
}

func getPayouts(r *http.Request) ([]string, []float64, error) {
	possiblePlaces := []string{"1", "2", "3", "4", "T5-6", "T7-8", "T9-12", "T13-16", "T17-24", "T25-32", "T33-48", "T49-64", "T65-96", "T97-128"}
	payouts := make([]float64, 0)
	places := make([]string, 0)

	for _, p := range possiblePlaces {
		str_rep := r.FormValue(fmt.Sprintf("Payout%s", p))
		if str_rep == "" {
			continue
		}
		val, err := strconv.ParseFloat(str_rep, 64)

		if err != nil {
			log.Error().Msg(fmt.Sprintf("Error parsing string %s to float for payout", p))
			return nil, nil, fmt.Errorf(fmt.Sprintf("Error parsing string %s to float for payout", p))
		}

		payouts = append(payouts, float64(val))
		places = append(places, p)
	}

	return places, payouts, nil
}
