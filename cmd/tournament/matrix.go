package tournament

import (
	"errors"
	"fmt"
	"math"

	"github.com/rs/zerolog/log"
)

type Matrix struct {
	counts [][]uint
}

func NewMatrix(round Round) (Matrix, error) {
	playerIds := make(map[uint]bool)
	playerIds[0] = true // always include the bye player
	maxId := uint(0)

	for _, player := range round.winnersSide {
		if playerIds[player.Id] && (player.Id != ByePlayerId) {
			err := fmt.Errorf("the player id %d already exists", player.Id)
			log.Err(err).Msg("Error creating places matrix")
			return Matrix{}, err
		}

		playerIds[player.Id] = true
		if player.Id > uint(maxId) {
			maxId = player.Id
		}
	}

	for _, player := range round.losersSide {
		if playerIds[player.Id] && (player.Id != ByePlayerId) {
			err := fmt.Errorf("the player id %d already exists", player.Id)
			log.Err(err).Msg("Error creating places matrix")
			return Matrix{}, err
		}

		playerIds[player.Id] = true
		if player.Id > uint(maxId) {
			maxId = player.Id
		}
	}

	if int(maxId) != len(playerIds)-1 {
		err := errors.New("unable to create players matrix, maxID value did not match the length of the provided input")
		log.Err(err).Msg("Error creating places matrix")
		return Matrix{}, err
	}

	numPlayers := maxId + 1
	numPlaces := uint(2*math.Log2(float64(len(round.winnersSide))) + 1.1)
	matrix := make([][]uint, numPlayers)

	for i := 0; i < int(numPlayers); i++ {
		matrix[i] = make([]uint, numPlaces)
	}
	return Matrix{matrix}, nil
}
func (m *Matrix) Probs(numRuns uint) ([][]float64, error) {
	ouputs := make([][]float64, len(m.counts))

	for i := 0; i < len(m.counts); i++ {
		row := make([]float64, len(m.counts[i]))
		sum := 0

		for j := 0; j < len(m.counts[i]); j++ {
			sum += int(m.counts[i][j])
			row[j] = float64(m.counts[i][j]) / float64(numRuns)
		}

		// check that the row probabilties add to 1, ignore this for the bye Player with index 0
		if (sum != int(numRuns)) && (i != ByePlayerId) {
			return nil, fmt.Errorf("expected %d total count in row %d. Got %d runs", numRuns, i, sum)
		}
		ouputs[i] = row
	}

	return ouputs, nil
}

// updatePlacesMatrix updates the given matrix based upon the values in places.
func (m *Matrix) Update(places [][]Player) error {
	for i := 0; i < len(places); i++ {
		for j := 0; j < len(places[i]); j++ {
			id2Update := places[i][j].Id

			if id2Update >= uint(len(m.counts)) {
				err := fmt.Errorf("out of bounds error for player ID %d when updating matrix with length %d", id2Update, len(m.counts))
				log.Err(err).Msg("Update places matrix error")
				return err
			}

			place2Update := uint(len(places) - i - 1)
			if uint(place2Update) >= uint(len(m.counts[i])) {
				err := fmt.Errorf("out of bounds error for places index %d when updating matrix with length %d", place2Update, len(m.counts[i]))
				log.Err(err).Msg("Update places matrix error")
				return err
			}

			m.counts[id2Update][place2Update]++
		}
	}

	return nil
}
