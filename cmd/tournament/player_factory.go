package tournament

import (
	"errors"
	"math"
	"strconv"

	"github.com/rs/zerolog/log"
)

const ByePlayerId = 0

// ByePlayer is a helper function that creates a dummy player for a bye round. This player must always
// lose their match unless playing against other ByePlayer.
func ByePlayer() Player {
	return Player{
		Id:    ByePlayerId,
		Fargo: -10000,
		Name:  "BYE",
	}
}

func FindFieldSize(numCurrPlayers int) int {
	currPow := math.Log2(float64(numCurrPlayers))
	ceilPow := math.Ceil(currPow)
	result := math.Pow(2, ceilPow)

	return int(math.Round(result))
}

func CreatePlayers(names []string, fargos []string) ([]Player, int, error) {
	if len(names) != len(fargos) {
		return nil, 0, errors.New("names and fargos must have the same length")
	}

	fieldSize := FindFieldSize(len(names))
	players := make([]Player, fieldSize)
	realPlayers := 0

	for i := 0; i < len(names); i++ {
		fargo, err := strconv.Atoi(fargos[i])

		if err != nil {
			log.Trace().Msgf("Was unable to parse fargo of '%s' to int for player name '%s'", fargos[i], names[i])
			players[i] = ByePlayer()
		} else {
			realPlayers++
			players[i] = Player{
				uint(realPlayers),
				fargo,
				names[i],
			}
		}
	}

	for i := len(names); i < fieldSize; i++ {
		players[i] = ByePlayer()
	}

	return players, realPlayers, nil
}
