package tournament

import "math"

type RaceCalculator func(int, int, bool) (uint, uint)

func CreateRaceCalc(winnerRace uint, loserRace uint, spotDiff uint, maxSpot uint, addRace bool) RaceCalculator {
	return func(a int, b int, isWinners bool) (uint, uint) {
		ratingsDiff := float64(a - b)
		spot := (ratingsDiff / float64(spotDiff))

		aSpot := 0
		bSpot := 0

		if a > b {
			aSpot = int(math.Floor(spot))
		} else {
			bSpot = int(math.Floor(-spot))
		}

		if aSpot > int(maxSpot) {
			aSpot = int(maxSpot)
		}

		if bSpot > int(maxSpot) {
			bSpot = int(maxSpot)
		}

		if !addRace {
			temp := aSpot
			aSpot = -bSpot
			bSpot = -temp
		}

		if isWinners {
			return uint(winnerRace) + uint(aSpot), uint(winnerRace) + uint(bSpot)
		}

		return uint(loserRace) + uint(aSpot), uint(loserRace) + uint(bSpot)
	}
}
