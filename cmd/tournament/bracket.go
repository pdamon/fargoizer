package tournament

import (
	"fmt"
	"math/rand"

	"github.com/rs/zerolog/log"
)

type Bracket struct {
	ws WinSelector
	rc RaceCalculator
}

func NewBracket(ws WinSelector, rc RaceCalculator) Bracket {
	return Bracket{
		ws, rc,
	}
}

// Run plays out a whole bracket.
// It takes in a round representing the starting state and the ordering of players dropping to the losers bracket.
// It returns a matrix with length n rounds where m[i] containers a list of the players eliminated in that round.
// It also can return an error.
func (b *Bracket) Run(round Round, losersOrders [][]uint32) ([][]Player, error) {
	var places [][]Player

	// run down to last three
	for i := 0; i < len(losersOrders); i++ {
		if len(round.winnersSide) <= 2 {
			break
		}
		// reorder players on the losers side
		reorderedLosers := make([]Player, len(round.losersSide))
		for j := 0; j < len(round.losersSide); j++ {
			reorderedLosers[j] = round.losersSide[losersOrders[i][j]]
		}

		round.losersSide = reorderedLosers

		results, err := round.Play(b.rc, b.ws)
		if err != nil {
			return nil, err
		}

		round.winnersSide = results.winners
		round.losersSide = results.losers

		places = append(places, results.firstOut)
		places = append(places, results.secondOut)
	}

	// run last three
	first, second, third, err := LastThree(round.winnersSide[0], round.winnersSide[1], round.losersSide[0], b.rc, b.ws)
	if err != nil {
		return nil, err
	}

	// output results
	places = append(places, []Player{third})
	places = append(places, []Player{second})
	places = append(places, []Player{first})

	return places, nil
}

func (b *Bracket) Sim(round Round, losersOrders [][]uint32, shuffle bool, numRounds uint) ([][]float64, error) {
	matrix, err := NewMatrix(round)
	if err != nil {
		log.Err(err).Msg("error creating places matrix in bracket simulation")
		return nil, err
	}

	for i := 0; i < int(numRounds); i++ {
		if shuffle {
			round.winnersSide, err = shufflePlayers(round.winnersSide)
			if err != nil {
				log.Err(err).Msg("error shuffling players")
				return nil, err
			}
		}
		places, err := b.Run(round, losersOrders)

		if err != nil {
			log.Err(err).Msg("error running the bracket")
			return nil, err
		}

		err = matrix.Update(places)
		if err != nil {
			log.Err(err).Msg("error updating places matrix")
			return nil, err
		}
	}

	probs, err := matrix.Probs(numRounds)
	if err != nil {
		log.Err(err).Msg("error converting counts to ratios when simulating bracket")
		return nil, err
	}

	return probs, nil
}

func shufflePlayers(inputPlayers []Player) ([]Player, error) {
	players := make([]Player, 0)

	for _, p := range inputPlayers {
		if p.Id != ByePlayerId {
			players = append(players, p)
		}
	}

	// log.Trace().Msgf("found %d players to shuffle", len(players))

	fieldSize := FindFieldSize(len(players))
	var order []int
	switch fieldSize {
	case 4:
		order = []int{0, 2, 3, 1}
	case 8:
		order = []int{0, 4, 6, 2, 3, 7, 5, 1}
	case 16:
		order = []int{0, 8, 12, 4, 6, 14, 10, 2, 3, 11, 15, 7, 5, 14, 9, 1}
	case 32:
		order = []int{0, 16, 24, 8, 12, 28, 20, 4, 6, 22, 30, 14, 10, 26, 18, 2, 3, 19, 27, 11, 15, 31, 23, 7, 5, 21, 29, 13, 9, 25, 17, 1}
	case 64:
		order = []int{0, 32, 48, 16, 24, 56, 40, 8, 12, 44, 60, 28, 20, 52, 36, 4, 6, 38, 54, 22, 30, 62, 46, 14, 10, 42, 58, 26, 18, 50, 34, 2, 3, 35, 51, 19, 27, 59, 43, 11, 15, 47, 63, 31, 23, 55, 39, 7, 5, 37, 53, 21, 29, 61, 45, 13, 9, 41, 57, 25, 17, 49, 33, 1}
	default:
		return nil, fmt.Errorf("invalid fieldSize found. got %d", fieldSize)
	}

	shuffled := make([]Player, fieldSize)
	rand.Shuffle(len(players), func(i, j int) { players[i], players[j] = players[j], players[i] })

	for i := 0; i < len(players); i++ {
		shuffled[order[i]] = players[i]
	}

	for i := len(players); i < fieldSize; i++ {
		shuffled[order[i]] = ByePlayer()
	}

	return shuffled, nil
}
