package tournament

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type AWins struct{}

func (a *AWins) PlayerAWins() bool {
	return true
}

func (a *AWins) Config(int, int) error {
	return nil
}

func raceToOne(int, int, bool) (uint, uint) {
	return 1, 1
}

const BRACKET_DELTA = 0.011

func TestRunBracketProps(t *testing.T) {
	type bracketTest struct {
		desc           string
		numRuns        uint
		input          Round
		numRealPlayers uint
		numPlaces      uint
		t              Bracket
		losersOrder    [][]uint32
		expectedRatios [][]float64
	}

	for _, tC := range []bracketTest{
		{
			"4 Equal Players",
			100_000,
			Round{
				[]Player{
					{Id: 1, Fargo: 0, Name: "A"},
					{Id: 2, Fargo: 0, Name: "B"},
					{Id: 3, Fargo: 0, Name: "C"},
					{Id: 4, Fargo: 0, Name: "D"},
				},
				[]Player{
					ByePlayer(),
					ByePlayer(),
				},
			},
			4,
			5,
			Bracket{
				NewRandWinSelector(1),
				raceToOne,
			},
			[][]uint32{
				{0, 1},
				{0},
			},
			[][]float64{
				{0.0, 0.0, 0.0, 0.0, 2.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
			},
		},
		{
			"4 Players Equal Races",
			100_000,
			Round{
				[]Player{
					{Id: 1, Fargo: 0, Name: "A"},
					{Id: 2, Fargo: 100, Name: "B"},
					{Id: 3, Fargo: 100, Name: "C"},
					{Id: 4, Fargo: 0, Name: "D"},
				},
				[]Player{
					ByePlayer(),
					ByePlayer(),
				},
			},
			4,
			5,
			Bracket{
				NewRandWinSelector(1),
				CreateRaceCalc(50, 50, 2, 1000, true),
			},
			[][]uint32{
				{0, 1},
				{0},
			},
			[][]float64{
				{0.0, 0.0, 0.0, 0.0, 2.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
			},
		},
	} {
		t.Run(tC.desc, func(t *testing.T) {
			matrix, err := NewMatrix(tC.input)
			if assert.NoError(t, err) {
				for i := 0; i < int(tC.numRuns); i++ {
					places, err := tC.t.Run(tC.input, tC.losersOrder)
					if assert.NoError(t, err) {
						matrix.Update(places)
					}
				}
				ratios, err := matrix.Probs(tC.numRuns)
				if assert.NoError(t, err) {
					assert.Equal(t, len(tC.expectedRatios), len(ratios))

					for i := 0; i < len(tC.expectedRatios); i++ {
						assert.InDeltaSlice(t, tC.expectedRatios[i], ratios[i], BRACKET_DELTA)
					}
				}
			}
		})
	}
}

func TestSimBracketProps(t *testing.T) {
	type bracketTest struct {
		desc           string
		numRuns        uint
		input          Round
		numRealPlayers uint
		numPlaces      uint
		t              Bracket
		losersOrder    [][]uint32
		expectedRatios [][]float64
		shuffle        bool
	}

	for _, tC := range []bracketTest{
		{
			"4 Equal Players",
			100_000,
			Round{
				[]Player{
					{Id: 1, Fargo: 0, Name: "A"},
					{Id: 2, Fargo: 0, Name: "B"},
					{Id: 3, Fargo: 0, Name: "C"},
					{Id: 4, Fargo: 0, Name: "D"},
				},
				[]Player{
					ByePlayer(),
					ByePlayer(),
				},
			},
			4,
			5,
			Bracket{
				NewRandWinSelector(1),
				raceToOne,
			},
			[][]uint32{
				{0, 1},
				{0},
			},
			[][]float64{
				{0.0, 0.0, 0.0, 0.0, 2.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
			},
			false,
		},
		{
			"4 UnEqual Players",
			100_000,
			Round{
				[]Player{
					{Id: 1, Fargo: 100, Name: "A"},
					{Id: 2, Fargo: 0, Name: "B"},
					{Id: 3, Fargo: 100, Name: "C"},
					{Id: 4, Fargo: 0, Name: "D"},
				},
				[]Player{
					ByePlayer(),
					ByePlayer(),
				},
			},
			4,
			5,
			Bracket{
				NewRandWinSelector(1),
				raceToOne,
			},
			[][]uint32{
				{0, 1},
				{0},
			},
			[][]float64{
				{0.0, 0.0, 0.0, 0.0, 2.0},
				{0.4, 0.29, 0.18, 0.13, 0.0},
				{0.1, 0.21, 0.32, 0.37, 0.0},
				{0.4, 0.29, 0.18, 0.13, 0.0},
				{0.1, 0.21, 0.32, 0.37, 0.0},
			},
			false,
		},
		{
			"4 Players Equal Races",
			100_000,
			Round{
				[]Player{
					{Id: 1, Fargo: 0, Name: "A"},
					{Id: 2, Fargo: 100, Name: "B"},
					{Id: 3, Fargo: 100, Name: "C"},
					{Id: 4, Fargo: 0, Name: "D"},
				},
				[]Player{
					ByePlayer(),
					ByePlayer(),
				},
			},
			4,
			5,
			Bracket{
				NewRandWinSelector(1),
				CreateRaceCalc(50, 50, 2, 1000, true),
			},
			[][]uint32{
				{0, 1},
				{0},
			},
			[][]float64{
				{0.0, 0.0, 0.0, 0.0, 2.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
				{0.25, 0.25, 0.25, 0.25, 0.0},
			},
			false,
		},
		{
			"4 Players Bad Draw",
			100_000,
			Round{
				[]Player{
					{Id: 1, Fargo: 100, Name: "A"},
					{Id: 2, Fargo: 100, Name: "B"},
					{Id: 3, Fargo: 0, Name: "C"},
					{Id: 4, Fargo: 0, Name: "D"},
				},
				[]Player{
					ByePlayer(),
					ByePlayer(),
				},
			},
			4,
			5,
			Bracket{
				NewRandWinSelector(1),
				raceToOne,
			},
			[][]uint32{
				{0, 1},
				{0},
			},
			[][]float64{
				{0.0, 0.0, 0.0, 0.0, 2.0},
				{0.38, 0.25, 0.20, 0.17, 0.0},
				{0.38, 0.25, 0.20, 0.17, 0.0},
				{0.12, 0.25, 0.295, 0.335, 0.0},
				{0.12, 0.25, 0.295, 0.335, 0.0},
			},
			false,
		},
		{
			"4 Bad Draw Shuffle",
			100_000,
			Round{
				[]Player{
					{Id: 1, Fargo: 100, Name: "A"},
					{Id: 2, Fargo: 100, Name: "B"},
					{Id: 3, Fargo: 0, Name: "C"},
					{Id: 4, Fargo: 0, Name: "D"},
				},
				[]Player{
					ByePlayer(),
					ByePlayer(),
				},
			},
			4,
			5,
			Bracket{
				NewRandWinSelector(1),
				raceToOne,
			},
			[][]uint32{
				{0, 1},
				{0},
			},
			[][]float64{
				{0.0, 0.0, 0.0, 0.0, 2.0},
				{0.39, 0.28, 0.19, 0.14, 0.0},
				{0.39, 0.28, 0.19, 0.14, 0.0},
				{0.11, 0.22, 0.31, 0.36, 0.0},
				{0.11, 0.22, 0.31, 0.36, 0.0},
			},
			true,
		},
	} {
		t.Run(tC.desc, func(t *testing.T) {
			ratios, err := tC.t.Sim(tC.input, tC.losersOrder, tC.shuffle, tC.numRuns)

			if assert.NoError(t, err) {
				assert.Equal(t, len(tC.expectedRatios), len(ratios))

				for i := 0; i < len(tC.expectedRatios); i++ {
					assert.InDeltaSlice(t, tC.expectedRatios[i], ratios[i], BRACKET_DELTA)
				}
			}
		})
	}
}
