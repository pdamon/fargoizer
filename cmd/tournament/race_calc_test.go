package tournament

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRaceCalc(t *testing.T) {
	type testRace struct {
		ratingA   int
		ratingB   int
		isWinners bool
		raceA     uint
		raceB     uint
	}

	type raceCalcTest struct {
		desc       string
		winnerRace uint
		loserRace  uint
		spotDiff   uint
		maxSpot    uint
		addRace    bool
		testCases  []testRace
	}

	for _, tC := range []raceCalcTest{
		{
			"Test 5/5 spot 1 per 100 Add", 5, 4, 100, 1, true,
			[]testRace{
				{0, 0, true, 5, 5},
				{0, 0, false, 4, 4},
				{100, 0, true, 6, 5},
				{200, 0, true, 6, 5},
				{0, 100, false, 4, 5},
			},
		},
		{
			"Test 5/5 spot 1 per 100 Sub", 5, 4, 100, 2, false,
			[]testRace{
				{0, 0, true, 5, 5},
				{0, 0, false, 4, 4},
				{100, 0, true, 5, 4},
				{200, 0, true, 5, 3},
				{0, 100, false, 3, 4},
			},
		},
	} {
		for _, tR := range tC.testCases {
			t.Run(fmt.Sprintf("%s-%d-%d-%t-%d-%d", tC.desc, tR.ratingA, tR.ratingB, tR.isWinners, tR.raceA, tR.raceB), func(t *testing.T) {
				calc := CreateRaceCalc(tC.winnerRace, tC.loserRace, tC.spotDiff, tC.maxSpot, tC.addRace)
				testRaceA, testRaceB := calc(tR.ratingA, tR.ratingB, tR.isWinners)
				assert.Equal(t, tR.raceA, testRaceA)
				assert.Equal(t, tR.raceB, testRaceB)
			})
		}
	}
}
