package tournament

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

const PLAYER_DELTA = 0.000001

func TestCalcWinRatios(t *testing.T) {
	type noErrorTestCase struct {
		description  string
		ratingA      int
		ratingB      int
		expectedProb float64
	}

	for _, scenario := range []noErrorTestCase{
		{
			description:  "Two players with same ratings",
			ratingA:      0,
			ratingB:      0,
			expectedProb: 0.5,
		},
		{
			description:  "Player B is 100 points higher",
			ratingA:      0,
			ratingB:      100,
			expectedProb: 0.333333,
		},
		{
			description:  "Player A is 100 points higher",
			ratingA:      100,
			ratingB:      0,
			expectedProb: 0.666667,
		},
		{
			description:  "Player B is 200 points higher",
			ratingA:      0,
			ratingB:      200,
			expectedProb: 0.2,
		},
		{
			description:  "Player A is 300 points higher",
			ratingA:      600,
			ratingB:      300,
			expectedProb: 0.888889,
		},
	} {
		t.Run(scenario.description, func(t *testing.T) {
			prob, err := CalcWinRatios(scenario.ratingA, scenario.ratingB)
			if assert.NoError(t, err) {
				assert.InDelta(t, scenario.expectedProb, prob, PLAYER_DELTA)
			}
		})
	}
}

type MockWinSelector struct {
	i       int
	winners []bool
}

func (mws *MockWinSelector) PlayerAWins() bool {
	if mws.i < len(mws.winners) {
		mws.i++
		return mws.winners[mws.i-1]
	}

	panic(fmt.Sprintf("Index %d out of range for number of games %d", mws.i, len(mws.winners)))
}

func (mws *MockWinSelector) Config(int, int) error {
	return nil
}

func NewMockWinSelector(results []bool) *MockWinSelector {
	return &MockWinSelector{
		0,
		results,
	}
}

func TestSimMatches(t *testing.T) {
	type testSimMatch struct {
		description string
		playerA     Player
		playerB     Player
		raceA       uint
		raceB       uint
		rand        WinSelector
		aWins       bool
	}

	for _, scenario := range []testSimMatch{
		{
			description: "Equal Ratings Winners Side",
			playerA:     Player{1, 0, "A"},
			playerB:     Player{2, 0, "B"},
			raceA:       5,
			raceB:       5,
			rand:        NewMockWinSelector([]bool{true, false, true, false, true, false, true, false, true, false, false, false}),
			aWins:       true,
		},
		{
			description: "Equal Ratings Winners Side",
			playerA:     Player{1, 0, "A"},
			playerB:     Player{2, 0, "B"},
			raceA:       7,
			raceB:       7,
			rand:        NewMockWinSelector([]bool{true, false, true, false, true, false, true, false, true, false, false, false}),
			aWins:       false,
		},
	} {
		t.Run(scenario.description, func(t *testing.T) {
			winner, loser, err := SimMatch(scenario.playerA, scenario.playerB, scenario.raceA, scenario.raceB, scenario.rand)
			if assert.NoError(t, err) {
				if scenario.aWins {
					assert.Equal(t, scenario.playerA, winner)
					assert.Equal(t, scenario.playerB, loser)
				} else {
					assert.Equal(t, scenario.playerA, loser)
					assert.Equal(t, scenario.playerB, winner)
				}
			}
		})
	}
}

// TestRandomWinSelector allows for testing of the random win selector.
// The test output values were selected by looking at the random values in the debugger, and selecting the correct ones.
func TestRandomWinSelector(t *testing.T) {
	type rwsTest struct {
		description string
		ratingA     int
		ratingB     int
		cutoff      float64
		sequence    []bool
	}

	rws := NewRandWinSelector(1)

	for _, scenario := range []rwsTest{
		{
			"Test Even",
			0,
			0,
			0.5,
			[]bool{false, false, false},
		},
		{
			"Test 100 Point Gap",
			100,
			0,
			0.666667,
			[]bool{true, true, false, true, true},
		},
	} {
		t.Run(scenario.description, func(t *testing.T) {
			randVals := make([]bool, len(scenario.sequence))
			rws.Config(scenario.ratingA, scenario.ratingB)
			assert.InDelta(t, scenario.cutoff, rws.cutoff, PLAYER_DELTA)

			for i := 0; i < len(scenario.sequence); i++ {
				randVals[i] = rws.PlayerAWins()
			}

			assert.Equal(t, scenario.sequence, randVals)
		})
	}
}

func TestUnequalRaces(t *testing.T) {
	type unEqRaceTest struct {
		desc     string
		playerA  Player
		playerB  Player
		raceCalc RaceCalculator
		ws       WinSelector
		expA     float64
		expB     float64
		numReps  uint
	}

	rws := NewRandWinSelector(1)
	localDelta := 0.01

	for _, tC := range []unEqRaceTest{
		{
			"Even Race 50",
			Player{1, 0, "A"},
			Player{2, 0, "B"},
			CreateRaceCalc(50, 50, 2, 1000, true),
			rws,
			0.5,
			0.5,
			10_000,
		},
		{
			"Unequal Race 50",
			Player{1, 0, "A"},
			Player{2, 100, "B"},
			CreateRaceCalc(50, 50, 2, 1000, true),
			rws,
			0.5,
			0.5,
			100_000,
		},
		{
			"Unequal Race 33",
			Player{1, 200, "A"},
			Player{2, 0, "B"},
			CreateRaceCalc(67, 67, 1, 1000, true),
			rws,
			0.5,
			0.5,
			100_000,
		},
	} {
		t.Run(tC.desc, func(t *testing.T) {
			raceA, raceB := tC.raceCalc(tC.playerA.Fargo, tC.playerB.Fargo, true)
			winsA := 0.0
			winsB := 0.0

			for i := 0; i < int(tC.numReps); i++ {
				winner, _, err := SimMatch(tC.playerA, tC.playerB, raceA, raceB, tC.ws)

				if assert.NoError(t, err) {
					if winner.Id == tC.playerA.Id {
						winsA++
					} else {
						winsB++
					}
				}
			}
			ratioA := winsA / float64(tC.numReps)
			ratioB := winsB / float64(tC.numReps)

			assert.InDelta(t, tC.expA, ratioA, localDelta)
			assert.InDelta(t, tC.expB, ratioB, localDelta)
		})
	}
}
