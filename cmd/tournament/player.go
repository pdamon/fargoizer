package tournament

import (
	"fmt"
	"math"
	"math/rand"
	"time"

	"github.com/rs/zerolog/log"
)

type Player struct {
	Id    uint
	Fargo int
	Name  string
}

type WinSelector interface {
	PlayerAWins() bool
	Config(int, int) error
}

type RandWinSelector struct {
	cutoff float64
	rand   *rand.Rand
}

func NewRandWinSelector(seed int64) *RandWinSelector {
	if seed == 0 {
		seed = time.Now().UnixNano()
	}

	return &RandWinSelector{
		cutoff: 0.5,
		rand:   rand.New(rand.NewSource(seed)),
	}
}

func (rws *RandWinSelector) Config(ratingA, ratingB int) error {
	co, err := CalcWinRatios(ratingA, ratingB)

	if err != nil {
		return err
	}

	rws.cutoff = co
	return nil
}

func (rws *RandWinSelector) PlayerAWins() bool {
	rv := rws.rand.Float64()

	return rv < rws.cutoff
}

// CalcWinRatios calculates the probability that playerA will win a single game
func CalcWinRatios(ratingA int, ratingB int) (float64, error) {
	prob := 1.0 / (math.Pow(2, float64(ratingB-ratingA)/100.0) + 1)

	if prob > 1.0 {
		err := fmt.Errorf("prob A wins was greater than 1 with ratings %d and %d", ratingA, ratingB)
		log.Err(err).Msg("Invalid win prob. Was greater than 1")
		return 0, err
	}

	if prob < 0.0 {
		err := fmt.Errorf("prob A wins was less than 0 with ratings %d and %d", ratingA, ratingB)
		log.Err(err).Msg("Invalid win prob. Was less than 0")
		return 0, err
	}

	if math.IsNaN(prob) {
		err := fmt.Errorf("prob A wins was NaN for ratings %d and %d", ratingA, ratingB)
		log.Err(err).Msgf("Got NaN win ratio")
		return 0.0, err
	}

	return prob, nil
}

// SimMatch simulates the given match.
//
// It returns (winning player, losing player, error)
func SimMatch(playerA Player, playerB Player, raceA uint, raceB uint, selector WinSelector) (Player, Player, error) {
	if playerA.Id == ByePlayerId {
		return playerB, playerA, nil
	}

	if playerB.Id == ByePlayerId {
		return playerA, playerB, nil
	}

	err := selector.Config(playerA.Fargo, playerB.Fargo)
	if err != nil {
		log.Err(err).Msgf("Error configuring win selector for match between %v+ and %v+", playerA, playerB)
		return Player{}, Player{}, err
	}

	var winsA uint = 0
	var winsB uint = 0

	for {
		if winsA >= raceA {
			return playerA, playerB, nil
		}

		if winsB >= raceB {
			return playerB, playerA, nil
		}

		if selector.PlayerAWins() {
			winsA += 1
		} else {
			winsB += 1
		}
	}
}
