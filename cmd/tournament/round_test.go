package tournament

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPlaySliceProps(t *testing.T) {
	type propsTest struct {
		desc           string
		players        []Player
		raceCalc       RaceCalculator
		rand           WinSelector
		expectedRatios [][]float64
		numRuns        uint
	}

	aw := &AWins{}
	rws := NewRandWinSelector(1)

	for _, tC := range []propsTest{
		{
			"4 Players A Wins",
			[]Player{
				{Id: 1, Fargo: 100, Name: "A"},
				{Id: 2, Fargo: 100, Name: "B"},
				{Id: 3, Fargo: 100, Name: "C"},
				{Id: 4, Fargo: 100, Name: "D"},
			},
			raceToOne,
			aw,
			[][]float64{
				{0.0, 0.0, 0.0, 0.0, 0.0},
				{1.0, 0.0, 0.0, 0.0, 0.0},
				{0.0, 0.0, 1.0, 0.0, 0.0},
				{0.0, 1.0, 0.0, 0.0, 0.0},
				{0.0, 0.0, 0.0, 1.0, 0.0},
			},
			10_000,
		},
		{
			"4 Equal Players",
			[]Player{
				{Id: 1, Fargo: 100, Name: "A"},
				{Id: 2, Fargo: 100, Name: "B"},
				{Id: 3, Fargo: 100, Name: "C"},
				{Id: 4, Fargo: 100, Name: "D"},
			},
			raceToOne,
			rws,
			[][]float64{
				{0.0, 0.0, 0.0, 0.0, 0.0},
				{0.5, 0.0, 0.5, 0.0, 0.0},
				{0.5, 0.0, 0.5, 0.0, 0.0},
				{0.0, 0.5, 0.0, 0.5, 0.0},
				{0.0, 0.5, 0.0, 0.5, 0.0},
			},
			10_000,
		},
	} {
		t.Run(tC.desc, func(t *testing.T) {
			matrix, err := NewMatrix(Round{tC.players, []Player{}})
			if assert.NoError(t, err) {
				for i := 0; i < int(tC.numRuns); i++ {
					winners, losers, err := playSlice(tC.players, tC.raceCalc, tC.rand)

					if assert.NoError(t, err) {
						for j := 0; j < len(winners); j++ {
							matrix.counts[winners[j].Id][j]++
						}
						for j := 0; j < len(losers); j++ {
							matrix.counts[losers[j].Id][j+2]++
						}
					}
				}

				ratios, err := matrix.Probs(tC.numRuns)
				if assert.NoError(t, err) {
					for i := 0; i < len(tC.expectedRatios); i++ {
						assert.InDeltaSlice(t, tC.expectedRatios[i], ratios[i], BRACKET_DELTA)
					}
				}
			}
		})
	}
}

func TestPlayLast3Props(t *testing.T) {
	type propsTest struct {
		desc           string
		players        []Player
		raceCalc       RaceCalculator
		rand           WinSelector
		expectedRatios [][]float64
		numRuns        uint
	}

	aw := &AWins{}
	rws := NewRandWinSelector(1)

	for _, tC := range []propsTest{
		{
			"3 Players A Wins",
			[]Player{
				{Id: 1, Fargo: 100, Name: "A"},
				{Id: 2, Fargo: 100, Name: "B"},
				{Id: 3, Fargo: 100, Name: "C"},
			},
			raceToOne,
			aw,
			[][]float64{
				{0.0, 0.0, 0.0},
				{1.0, 0.0, 0.0},
				{0.0, 1.0, 0.0},
				{0.0, 0.0, 1.0},
			},
			100_000,
		},
		{
			"3 Players Random Winners",
			[]Player{
				{Id: 1, Fargo: 100, Name: "A"},
				{Id: 2, Fargo: 100, Name: "B"},
				{Id: 3, Fargo: 100, Name: "C"},
			},
			raceToOne,
			rws,
			[][]float64{
				{0.0, 0.0, 0.0},
				{0.4375, 0.3125, 0.25},
				{0.4375, 0.3125, 0.25},
				{0.125, 0.375, 0.5},
			},
			1_000_000,
		},
		{
			"2 Players Bye Losers",
			[]Player{
				{Id: 1, Fargo: 100, Name: "A"},
				{Id: 2, Fargo: 100, Name: "B"},
				{Id: 0, Fargo: 0, Name: "BYE"},
			},
			raceToOne,
			rws,
			[][]float64{
				{0.0, 0.0, 1.0},
				{0.5, 0.5, 0.0},
				{0.5, 0.5, 0.0},
			},
			1_000_000,
		},
		{
			"2 Players Bye Winners",
			[]Player{
				{Id: 1, Fargo: 100, Name: "A"},
				{Id: 0, Fargo: 0, Name: "BYE"},
				{Id: 2, Fargo: 100, Name: "B"},
			},
			raceToOne,
			rws,
			[][]float64{
				{0.0, 0.0, 1.0},
				{0.75, 0.25, 0.0},
				{0.25, 0.75, 0.0},
			},
			1_000_000,
		},
	} {
		t.Run(tC.desc, func(t *testing.T) {
			matrix, err := NewMatrix(Round{tC.players[0:2], []Player{tC.players[2]}})
			if assert.NoError(t, err) {
				for i := 0; i < int(tC.numRuns); i++ {
					first, second, third, err := LastThree(tC.players[0], tC.players[1], tC.players[2], raceToOne, tC.rand)
					if assert.NoError(t, err) {
						matrix.counts[first.Id][0]++
						matrix.counts[second.Id][1]++
						matrix.counts[third.Id][2]++
					}
				}

				ratios, err := matrix.Probs(tC.numRuns)
				if assert.NoError(t, err) {
					for i := 0; i < len(tC.expectedRatios); i++ {
						assert.InDeltaSlice(t, tC.expectedRatios[i], ratios[i], BRACKET_DELTA)
					}
				}
			}
		})
	}
}

func TestPlayRoundProps(t *testing.T) {
	type propsTest struct {
		desc           string
		round          Round
		tournament     Bracket
		expectedRatios [][]float64
		numRuns        uint
	}

	aw := &AWins{}
	rws := NewRandWinSelector(1)

	for _, tC := range []propsTest{
		{
			"6 Players A Wins",
			Round{
				[]Player{
					{Id: 1, Fargo: 100, Name: "A"},
					{Id: 2, Fargo: 100, Name: "B"},
					{Id: 3, Fargo: 100, Name: "C"},
					{Id: 4, Fargo: 100, Name: "D"},
				},
				[]Player{
					{Id: 5, Fargo: 100, Name: "E"},
					{Id: 6, Fargo: 100, Name: "F"},
				}},
			Bracket{
				aw,
				raceToOne,
			},
			[][]float64{
				{0.0, 0.0, 0.0, 0.0, 0.0},
				{1.0, 0.0, 0.0, 0.0, 0.0},
				{0.0, 1.0, 0.0, 0.0, 0.0},
				{1.0, 0.0, 0.0, 0.0, 0.0},
				{0.0, 0.0, 1.0, 0.0, 0.0},
				{0.0, 0.0, 0.0, 1.0, 0.0},
				{0.0, 0.0, 0.0, 1.0, 0.0},
			},
			100_000,
		},
		{
			"6 Players Random Winners",
			Round{
				[]Player{
					{Id: 1, Fargo: 100, Name: "A"},
					{Id: 2, Fargo: 100, Name: "B"},
					{Id: 3, Fargo: 100, Name: "C"},
					{Id: 4, Fargo: 100, Name: "D"},
				},
				[]Player{
					{Id: 5, Fargo: 100, Name: "E"},
					{Id: 6, Fargo: 100, Name: "F"},
				}},
			Bracket{
				rws,
				raceToOne,
			},
			[][]float64{
				{0.0, 0.0, 0.0, 0.0, 0.0},
				{0.5, 0.125, 0.125, 0.25, 0.0},
				{0.5, 0.125, 0.125, 0.25, 0.0},
				{0.5, 0.125, 0.125, 0.25, 0.0},
				{0.5, 0.125, 0.125, 0.25, 0.0},
				{0.0, 0.25, 0.25, 0.5, 0.0},
				{0.0, 0.25, 0.25, 0.5, 0.0},
			},
			1_000_000,
		},
		{
			"4 Players Winners Byes",
			Round{
				[]Player{
					{Id: 1, Fargo: 100, Name: "A"},
					{Id: 0, Fargo: 100, Name: "BYE"},
					{Id: 2, Fargo: 100, Name: "C"},
					{Id: 0, Fargo: 100, Name: "BYE"},
				},
				[]Player{
					{Id: 3, Fargo: 100, Name: "E"},
					{Id: 4, Fargo: 100, Name: "F"},
				}},
			Bracket{
				rws,
				raceToOne,
			},
			[][]float64{
				{0.0, 0.0, 0.0, 2.0, 0.0},
				{1.0, 0.0, 0.0, 0.0, 0.0},
				{1.0, 0.0, 0.0, 0.0, 0.0},
				{0.0, 0.5, 0.5, 0.0, 0.0},
				{0.0, 0.5, 0.5, 0.0, 0.0},
			},
			1_000_000,
		},
		{
			"4 Players Losers Byes",
			Round{
				[]Player{
					{Id: 1, Fargo: 100, Name: "A"},
					{Id: 2, Fargo: 100, Name: "B"},
					{Id: 3, Fargo: 100, Name: "C"},
					{Id: 4, Fargo: 100, Name: "D"},
				},
				[]Player{
					{Id: 0, Fargo: 0, Name: "BYE"},
					{Id: 0, Fargo: 0, Name: "BYE"},
				}},
			Bracket{
				rws,
				raceToOne,
			},
			[][]float64{
				{0.0, 0.0, 0.0, 2.0, 0.0},
				{0.5, 0.25, 0.25, 0.0, 0.0},
				{0.5, 0.25, 0.25, 0.0, 0.0},
				{0.5, 0.25, 0.25, 0.0, 0.0},
				{0.5, 0.25, 0.25, 0.0, 0.0},
			},
			1_000_000,
		},
	} {
		t.Run(tC.desc, func(t *testing.T) {
			matrix, err := NewMatrix(tC.round)
			if assert.NoError(t, err) {
				for i := 0; i < int(tC.numRuns); i++ {
					result, err := tC.round.Play(tC.tournament.rc, tC.tournament.ws)
					if assert.NoError(t, err) {
						for j := 0; j < len(result.winners); j++ {
							matrix.counts[result.winners[j].Id][0]++
						}
						for j := 0; j < len(result.losers); j++ {
							matrix.counts[result.losers[j].Id][1]++
						}

						for j := 0; j < len(result.secondOut); j++ {
							matrix.counts[result.secondOut[j].Id][2]++
						}
						for j := 0; j < len(result.firstOut); j++ {
							matrix.counts[result.firstOut[j].Id][3]++
						}
					}
				}

				ratios, err := matrix.Probs(tC.numRuns)

				if assert.NoError(t, err) {
					for i := 0; i < len(tC.expectedRatios); i++ {
						assert.InDeltaSlice(t, tC.expectedRatios[i], ratios[i], BRACKET_DELTA)
					}
				}
			}
		})
	}
}
