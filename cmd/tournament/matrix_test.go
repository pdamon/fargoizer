package tournament

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProbs(t *testing.T) {
	type countsTest struct {
		desc     string
		counts   [][]uint
		expected [][]float64
		numRuns  uint
	}

	for _, tC := range []countsTest{
		{
			"Test 4 Counts",
			[][]uint{
				{2, 1, 0, 1},
				{0, 3, 0, 1},
				{2, 0, 2, 0},
				{0, 0, 2, 2},
			},
			[][]float64{
				{0.5, 0.25, 0.0, 0.25},
				{0.0, 0.75, 0.0, 0.25},
				{0.5, 0.0, 0.5, 0.0},
				{0.0, 0.0, 0.5, 0.5},
			},
			4,
		},
	} {
		t.Run(tC.desc, func(t *testing.T) {
			matrix := Matrix{tC.counts}
			ratios, err := matrix.Probs(tC.numRuns)

			if assert.NoError(t, err) {
				assert.Equal(t, tC.expected, ratios)
			}
		})
	}
}

func TestUpdatePlacesMatrix(t *testing.T) {
	type matrixTest struct {
		desc     string
		places   [][]Player
		expected [][]uint
	}

	matrix, err := NewMatrix(Round{
		[]Player{
			{Id: 1, Fargo: 0, Name: "A"},
			{Id: 2, Fargo: 0, Name: "B"},
			{Id: 3, Fargo: 0, Name: "C"},
			{Id: 4, Fargo: 0, Name: "D"},
			{Id: 5, Fargo: 0, Name: "E"},
			{Id: 6, Fargo: 0, Name: "F"},
			{Id: 7, Fargo: 0, Name: "G"},
			{Id: 8, Fargo: 0, Name: "H"},
			{Id: 9, Fargo: 0, Name: "I"},
			{Id: 10, Fargo: 0, Name: "J"},
			{Id: 11, Fargo: 0, Name: "K"},
			{Id: 12, Fargo: 0, Name: "L"},
			{Id: 13, Fargo: 0, Name: "M"},
			{Id: 14, Fargo: 0, Name: "N"},
			{Id: 15, Fargo: 0, Name: "O"},
			{Id: 16, Fargo: 0, Name: "P"},
		},
		[]Player{},
	})

	if assert.NoError(t, err) {
		for _, tC := range []matrixTest {
			{
				"Test First Update",
				[][]Player{
					{
						{Id: 1, Fargo: 0, Name: "A"},
						{Id: 2, Fargo: 0, Name: "B"},
						{Id: 3, Fargo: 0, Name: "C"},
						{Id: 4, Fargo: 0, Name: "D"},
					},
					{
						{Id: 5, Fargo: 0, Name: "E"},
						{Id: 6, Fargo: 0, Name: "F"},
						{Id: 7, Fargo: 0, Name: "G"},
						{Id: 8, Fargo: 0, Name: "H"},
					},
					{
						{Id: 9, Fargo: 0, Name: "I"},
						{Id: 10, Fargo: 0, Name: "J"},
					},
					{
						{Id: 11, Fargo: 0, Name: "K"},
						{Id: 12, Fargo: 0, Name: "L"},
					},
					{
						{Id: 13, Fargo: 0, Name: "M"},
					},
					{
						{Id: 14, Fargo: 0, Name: "N"},
					},
					{
						{Id: 15, Fargo: 0, Name: "O"},
					},
					{
						{Id: 16, Fargo: 0, Name: "P"},
					},
				},
				[][]uint{
					{0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 1, 0},
					{0, 0, 0, 0, 0, 0, 0, 1, 0},
					{0, 0, 0, 0, 0, 0, 0, 1, 0},
					{0, 0, 0, 0, 0, 0, 0, 1, 0},
					{0, 0, 0, 0, 0, 0, 1, 0, 0},
					{0, 0, 0, 0, 0, 0, 1, 0, 0},
					{0, 0, 0, 0, 0, 0, 1, 0, 0},
					{0, 0, 0, 0, 0, 0, 1, 0, 0},
					{0, 0, 0, 0, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 0, 0, 0, 0},
					{0, 0, 0, 1, 0, 0, 0, 0, 0},
					{0, 0, 1, 0, 0, 0, 0, 0, 0},
					{0, 1, 0, 0, 0, 0, 0, 0, 0},
					{1, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
		} {
			t.Run(tC.desc, func(t *testing.T) {
				err := matrix.Update(tC.places)

				if assert.NoError(t, err) {
					assert.Equal(t, tC.expected, matrix.counts)
				}
			})
		}
	}
}
