package tournament

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreatePlayers(t *testing.T) {
	type playerFactoryTest struct {
		desc        string
		names       []string
		fargos      []string
		realPlayers int
		expected    []Player
	}

	for _, scenario := range []playerFactoryTest{
		{
			"Three Player Input",
			[]string{"A", "B", "C"},
			[]string{"100", "0", "50"},
			3,
			[]Player{
				{1, 100, "A"},
				{2, 0, "B"},
				{3, 50, "C"},
				{0, -10_000, "BYE"},
			},
		},
		{
			"Four Players with a bye",
			[]string{"A", "BYE", "C", "D"},
			[]string{"100", "lol", "-10", "12"},
			3,
			[]Player{
				{1, 100, "A"},
				{0, -10_000, "BYE"},
				{2, -10, "C"},
				{3, 12, "D"},
			},
		},
	} {
		t.Run(scenario.desc, func(t *testing.T) {
			players, num, err := CreatePlayers(scenario.names, scenario.fargos)

			if assert.NoError(t, err) {
				assert.Equal(t, scenario.expected, players)
				assert.Equal(t, scenario.realPlayers, num)
			}
		})
	}
}

