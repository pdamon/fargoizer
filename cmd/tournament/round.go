package tournament

import (
	"fmt"

	"github.com/rs/zerolog/log"
)

type Round struct {
	winnersSide []Player
	losersSide  []Player
}

type RoundResult struct {
	winners   []Player
	losers    []Player
	firstOut  []Player
	secondOut []Player
}

func NewRound(w []Player, l []Player) Round {
	return Round{w, l}
}

func (r *Round) NumWinners() int {
	return len(r.winnersSide)
}

// playSlice plays out matches between pairs of players in the slice.
// It returns `(winners, losers)` where `winners[0]` is the winner of the match between `players[0]` and `players[1]`.
// `losers[0]` is the corresponding loser of the match between `players[0]` and `players[1]`.
func playSlice(playersSlice []Player, raceCalc RaceCalculator, ws WinSelector) ([]Player, []Player, error) {
	if len(playersSlice)%2 != 0 {
		return nil, nil, fmt.Errorf("length of players must be even, but got length %d", len(playersSlice))
	}

	outputSize := len(playersSlice) / 2
	winners := make([]Player, outputSize)
	losers := make([]Player, outputSize)

	for i := 0; i < len(playersSlice)/2; i++ {
		playerA := playersSlice[2*i]
		playerB := playersSlice[2*i+1]

		raceA, raceB := raceCalc(playerA.Fargo, playerB.Fargo, true)
		winner, loser, err := SimMatch(playerA, playerB, raceA, raceB, ws)

		if err != nil {
			return nil, nil, err
		}

		winners[i] = winner
		losers[i] = loser
	}

	return winners, losers, nil
}

// Play plays out the given round. It takes in the piayers on the winners and losers side.
// It returns the winners, losers, first out, second out, and any errors
func (r Round) Play(rc RaceCalculator, ws WinSelector) (RoundResult, error) {
	// Player the winners side
	if len(r.winnersSide) != 2*len(r.losersSide) {
		return RoundResult{}, fmt.Errorf("len of winners side must to 2*len losers side. Got %d and %d", len(r.winnersSide), len(r.losersSide))
	}

	winners, oneLosses, err := playSlice(r.winnersSide, rc, ws)
	if err != nil {
		log.Err(err).Msg("There was an error simming the winners side")
		return RoundResult{}, err
	}

	// play the drop round
	if len(oneLosses) != len(r.losersSide) {
		return RoundResult{}, fmt.Errorf("len of one losses must equal len of losers side. Got %d and %d", len(oneLosses), len(r.losersSide))
	}

	losersSide := make([]Player, len(oneLosses)*2)
	for i := 0; i < len(oneLosses); i++ {
		losersSide[2*i] = oneLosses[i]
		losersSide[2*i+1] = r.losersSide[i]
	}

	losersWinners, firstOut, err := playSlice(losersSide, rc, ws)
	if err != nil {
		log.Err(err).Msg("Error simming first round losers side")
	}

	// play the sqish round
	survived, secondOut, err := playSlice(losersWinners, rc, ws)
	if err != nil {
		log.Err(err).Msg("Error simming losers side squish round")
	}

	if (len(winners) != 2*len(survived)) || (len(firstOut) != 2*len(secondOut)) || (len(survived) != len(secondOut)) {
		return RoundResult{}, fmt.Errorf("there was in error in the lengths of the winners, survied, 1st out, and 2nd out. Got %d, %d, %d, %d", len(winners), len(survived), len(firstOut), len(secondOut))
	}

	return RoundResult{winners, survived, firstOut, secondOut}, nil
}

// playLastThree plays out the end of a tournament, between two players in the hotseat match,
// and one who has made it to the losers finals.
func LastThree(winnerA Player, winnerB Player, loser Player, rc RaceCalculator, ws WinSelector) (Player, Player, Player, error) {
	// sim the hotseat match
	raceWA, raceWB := rc(winnerA.Fargo, winnerB.Fargo, true)
	hotseat, firstLoss, err := SimMatch(winnerA, winnerB, raceWA, raceWB, ws)
	if err != nil {
		return Player{}, Player{}, Player{}, err
	}

	// sim losers finals
	raceFL, raceL := rc(firstLoss.Fargo, loser.Fargo, false)
	backdoorplayer, third, err := SimMatch(firstLoss, loser, raceFL, raceL, ws)
	if err != nil {
		return Player{}, Player{}, Player{}, err
	}

	// sim finals
	raceH, raceBD := rc(hotseat.Fargo, backdoorplayer.Fargo, true)
	winFinals, loseFinals, err := SimMatch(hotseat, backdoorplayer, raceH, raceBD, ws)
	if err != nil {
		return Player{}, Player{}, Player{}, err
	}

	// check and sim double dip
	if hotseat == winFinals {
		return winFinals, loseFinals, third, nil
	}

	// double dip finals
	raceWF, raceLF := rc(winFinals.Fargo, loseFinals.Fargo, false)
	first, second, err := SimMatch(winFinals, loseFinals, raceWF, raceLF, ws)
	if err != nil {
		return Player{}, Player{}, Player{}, err
	}

	return first, second, third, nil
}

