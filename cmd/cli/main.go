package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"math"
	"os"
	"strconv"

	"github.com/rs/zerolog/log"
	"gitlab.com/pdamon/fargoizer/cmd/htmx"
	"gitlab.com/pdamon/fargoizer/cmd/tournament"
)

const defaultSims = 1_000_000
const defaultSeed = 0
const defaultSpotDiff = 100
const defaultMaxSpot = 0
const defaultLoserRace = 5
const defaultWinnerRace = 5
const defaultAddRace = true

func main() {
	input := flag.String("input", "", "The path to the input file")
	output := flag.String("output", "", "The path to the output file")
	// output := flag.String("output", "", "The path to the output file")
	numSims := flag.Uint("sims", defaultSims, "The number of sims to run")
	seed := flag.Uint("seed", defaultSeed, "The random seed")

	winRace := flag.Uint("winrace", defaultWinnerRace, "The winners side race")
	loserRace := flag.Uint("loserace", defaultLoserRace, "The losers side race")
	maxSpot := flag.Uint("maxspot", defaultMaxSpot, "The maximum spot")
	spotDiff := flag.Uint("spotdiff", defaultSpotDiff, "The fargo diff per spot")
	addRace := flag.Bool("addrace", defaultAddRace, "Whether to add or subtract the race")

	flag.Parse()
	log.Trace().Msgf("value of --input is '%s'", *input)
	log.Trace().Msgf("value of --addrace is '%t'", *addRace)

	players, payouts, err := readCSV(*input)
	if err != nil {
		log.Err(err).Msg("error reading csv file")
		log.Fatal().Msg("exiting")
	}

	ws := tournament.NewRandWinSelector(int64(*seed))
	rc := tournament.CreateRaceCalc(*winRace, *loserRace, *spotDiff, *maxSpot, *addRace)

	tr := tournament.NewBracket(ws, rc)

	rd := makeInputRound(players)
	lo, err := defaultLosersOrders(uint(rd.NumWinners()))
	if err != nil {
		log.Err(err).Msg("error making losers orders")
		log.Fatal().Msg("exiting")
	}

	printPayouts(payouts)

	simResults, err := tr.Sim(rd, lo, true, *numSims)
	if err != nil {
		log.Err(err).Msg("there was an error running the sim")
		log.Fatal().Msg("exiting")
	}

	evs, err := htmx.CalcEVs(simResults, payouts)
	if err != nil {
		log.Err(err).Msg("there was an error calculating the evs")
		log.Fatal().Msg("exiting")
	}

	sortedPlayers := htmx.SortPlayersByEv(players, evs)

	printEvs(sortedPlayers, evs)

	if *output != "" {
		err = writeToFile(sortedPlayers, evs, simResults, *output)
		if err != nil {
			log.Err(err).Msg("error writing to the output file")
			log.Fatal().Msg("exiting")
		}
	}
}

func printEvs(players []tournament.Player, evs []float64) error {
	// if len(players) >= len(evs) {
	// 	err := fmt.Errorf("the lengths of players and evs did not match. they were %d and %d", len(players), len(evs))
	// 	log.Err(err).Msg("error printing evs")
	// 	return err
	// }

	fmt.Print("-------------------------- PLAYER EVS -----------------------------\n")
	rank := 1
	for _, p := range players {
		if p.Id != tournament.ByePlayerId {
			if p.Id > uint(len(evs)) {
				err := fmt.Errorf("got invalid player id %d", p.Id)
				log.Err(err).Msg("error printing evs")
				return err
			}

			fmt.Printf("%d: %s -> %d, $%.2f\n", rank, p.Name, p.Fargo, evs[p.Id])
			rank++
		}
	}

	return nil
}

func writeToFile(players []tournament.Player, evs []float64, pcts [][]float64, path string) error {
	// if len(players) >= len(evs) {
	// 	err := fmt.Errorf("the lengths of players and evs did not match. they were %d and %d", len(players), len(evs))
	// 	log.Err(err).Msg("error printing evs")
	// 	return err
	// }

	records := make([][]string, 0)

	for _, p := range players {
		if p.Id != tournament.ByePlayerId {
			rec := make([]string, 0)
			rec = append(rec, p.Name)
			rec = append(rec, strconv.FormatInt(int64(p.Fargo), 10))
			rec = append(rec, strconv.FormatFloat(evs[p.Id], 'f', 2, 64))

			for _, pct := range pcts[p.Id] {
				rec = append(rec, strconv.FormatFloat(pct, 'f', 4, 64))
			}

			records = append(records, rec)
		}
	}

	file, err := os.Create(path)
	if err != nil {
		log.Err(err).Msg("error opening output file")
		return err
	}

	writer := csv.NewWriter(file)
	err = writer.WriteAll(records)
	if err != nil {
		log.Err(err).Msg("error writing to output file")
		return err
	}

	return nil
}

func printPayouts(payouts []float64) error {
	possiblePlaces := []string{"1", "2", "3", "4", "T5-6", "T7-8", "T9-12", "T13-16", "T17-24", "T25-32", "T33-48", "T49-64"}

	if len(payouts) > len(possiblePlaces) {
		return fmt.Errorf("got too many payouts values. you can only pay up to 64th place")
	}

	for i, p := range payouts {
		fmt.Printf("places %s: $%.2f\n", possiblePlaces[i], p)
	}

	return nil
}

func defaultLosersOrders(players uint) ([][]uint32, error) {
	switch players {
	case 4:
		return [][]uint32{{0, 1}, {0}}, nil
	case 8:
		return [][]uint32{{0, 1, 2, 3}, {1, 0}, {0}}, nil
	case 16:
		return [][]uint32{
			{0, 1, 2, 3, 4, 5, 6, 7},
			{3, 2, 1, 0},
			{0, 1},
			{0},
		}, nil
	case 32:
		return [][]uint32{
			{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
			{4, 5, 6, 7, 0, 1, 2, 3},
			{0, 1, 2, 3},
			{1, 0},
			{0},
		}, nil
	case 64:
		return [][]uint32{
			{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 10, 21, 22, 23, 24, 25, 26, 27, 28, 29, 20, 31},
			{15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
			{3, 2, 1, 0, 7, 6, 5, 4},
			{2, 3, 1, 0},
			{1, 0},
			{0},
		}, nil
	// case 128:
	// case 256:
	default:
		return nil, fmt.Errorf("got invalid number of players %d. must be a power of 2 between 4 and 128", players)
	}

}

func makeInputRound(players []tournament.Player) tournament.Round {
	winnersSize := tournament.FindFieldSize(len(players))
	losersSize := winnersSize / 2

	initByes := make([]tournament.Player, losersSize)
	for i := 0; i < losersSize; i++ {
		initByes[i] = tournament.ByePlayer()
	}

	initWinners := make([]tournament.Player, winnersSize)
	copy(initWinners, players)

	for i := len(players); i < winnersSize; i++ {
		initWinners[i] = tournament.ByePlayer()
	}

	return tournament.NewRound(initWinners, initByes)
}

func readCSV(path string) ([]tournament.Player, []float64, error) {
	f, err := os.Open(path)
	if err != nil {
		log.Err(err).Msgf("unable to read file %s", path)
		return nil, nil, err
	}

	defer f.Close()

	csvReader := csv.NewReader(f)
	data, err := csvReader.ReadAll()
	if err != nil {
		log.Err(err).Msg("error parsing csv")
		return nil, nil, err
	}

	names := make([]string, len(data))
	fargos := make([]string, len(data))
	payouts := make([]float64, 0)

	for i := 0; i < len(data); i++ {
		names[i] = data[i][0]
		fargos[i] = data[i][1]

		if data[i][2] != "" {
			payout, err := strconv.ParseFloat(data[i][2], 64)
			if err != nil {
				log.Err(err).Msg("error parsing payout")
				return nil, nil, err
			}
			payouts = append(payouts, payout)
		}
	}

	players, _, err := tournament.CreatePlayers(names, fargos)
	if err != nil {
		log.Err(err).Msg("error creating players")
	}

	numPlaces := uint(2*math.Log2(float64(tournament.FindFieldSize(len(players)))) + 1.1)
	payoutsPlus := make([]float64, numPlaces)
	copy(payoutsPlus, payouts)

	return players, payoutsPlus, nil
}
