registry_url := registry.gitlab.com/pdamon/fargoizer
current_ci_tag := 0.0.4
current_test_tag := 0.0.2
current_prod_tag := 0.0.1
current_dind_tag := 0.0.1

check-format: build-templ
	test -z $(gofmt -l .)
	go vet ./cmd

docker-build-ci:
	docker build -f docker/Dockerfile-ci -t $(registry_url)/ci:$(current_ci_tag) .
	docker build -f docker/Dockerfile-ci -t $(registry_url)/ci:latest .

docker-push-ci: docker-build-ci
	docker push $(registry_url)/ci:$(current_ci_tag)
	docker push $(registry_url)/ci:latest

docker-build-test:
	docker build -f docker/Dockerfile -t $(registry_url)/test:$(current_test_tag) .
	docker build -f docker/Dockerfile -t $(registry_url)/test:latest .

docker-push-test: docker-build-test
	docker push $(registry_url)/test:$(current_test_tag)
	docker push $(registry_url)/test:latest

docker-build-dind:
	docker build -f docker/Dockerfile-dind -t $(registry_url)/dind:$(current_dind_tag) .
	docker build -f docker/Dockerfile-dind -t $(registry_url)/dind:latest .

docker-push-dind: docker-build-dind
	docker push $(registry_url)/dind:$(current_dind_tag)
	docker push $(registry_url)/dind:latest

docker-build-prod:
	docker build -f docker/Dockerfile -t $(registry_url)/prod:$(current_prod_tag) .
	docker build -f docker/Dockerfile -t $(registry_url)/prod:latest .

docker-push-prod: docker-build-prod
	docker push $(registry_url)/prod:$(current_prod_tag)
	docker push $(registry_url)/prod:latest

clean:
	rm -rf output
	rm -rf coverage
	rm -rf binaries
	rm -rf test_files/outputs/*
	rm -f fargoizer
	rm -f cmd/*_templ.go

#build-docs:

setup-folders:
	mkdir -p binaries/unit-tests
	mkdir -p binaries/test-binaries
	mkdir -p binaries/prod-binaries
	mkdir -p coverage/int
	mkdir -p coverage/unit
	mkdir -p test_files/outputs

build-templ:
	templ generate

build-binary: build-templ setup-folders
	go build -o binaries/prod-binaries ./cmd/...

build-unit-tests: build-templ setup-folders
	go test -c -cover -o binaries/unit-tests ./cmd/...

build-test-binary: build-templ setup-folders
	go build -cover -o binaries/test-binaries ./cmd/...

build: build-templ build-binary build-unit-tests build-test-binary

unit-test: setup-folders
	./binaries/unit-tests/tournament.test -test.gocoverdir="${PWD}/coverage/unit"

integration-test: setup-folders
	GOCOVERDIR=coverage/int ./test_files/e2e_test.sh

test: unit-test integration-test coverage-report

coverage-report: setup-folders
	go tool covdata textfmt -i=coverage/int,coverage/unit -o coverage/profile
	go tool covdata percent -i=coverage/int,coverage/unit
	go tool cover -html=coverage/profile -o coverage/coverage.html

coverage-check:
	test_files/check_coverage.sh "10.0"
