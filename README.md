# fargoizer

This is a library to simulate a double elimination bracket based upon players ELO rankings, and determine the probability that each player wins the tournament.
