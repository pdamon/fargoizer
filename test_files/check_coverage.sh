#!/bin/bash

COV="$(go tool covdata percent -i=coverage/int,coverage/unit | grep -Eo '[0-9.]{2,10}')"

echo "Have ${COV}% coverage. Needed $1%"

if [ "$(echo "${COV} > $1" | bc)" -eq 1 ]; then
  echo "PASSED!"
  exit 0
fi

echo "Test Failed!"
exit 1