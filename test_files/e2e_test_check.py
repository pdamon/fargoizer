#!/usr/bin/env python

import sys
import pandas as pd
from pandas.testing import assert_frame_equal


def main():
    path_control = sys.argv[1]
    path_test = sys.argv[2]
    
    control_file = pd.read_csv(path_control, header=None, index_col=[0]).sort_index()
    test_file = pd.read_csv(path_test, header=None, index_col=[0]).sort_index()

    assert_frame_equal(control_file, test_file, rtol=0.03, atol=0.03)


if __name__ == '__main__':
    main()
