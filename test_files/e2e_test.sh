#!/usr/bin/env bash
set -e

./binaries/test-binaries/cli --input ./test_files/inputs/2023_players.csv --output ./test_files/outputs/2023_output.csv --winrace 5 --loserace 5 --maxspot 2 --spotdiff 100 --addrace=false

./test_files/e2e_test_check.py ./test_files/outputs/2023_output.csv ./test_files/expected/2023_output.csv
